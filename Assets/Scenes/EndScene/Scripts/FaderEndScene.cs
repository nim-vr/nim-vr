using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FaderEndScene : MonoBehaviour
{
    [SerializeField]
    Image fadeToBlack;
    [SerializeField]
    Transform player;

    float startTime;

    void Start()
    {
        startTime = Time.time;
    }

    void Update()
    {
        fadeToBlack.color = new Color(0f, 0f, 0f, 1 - (Time.time + startTime)/2);
    }
}
