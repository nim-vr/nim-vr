using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class storyline : MonoBehaviour
{
    [SerializeField]
    Image fadeToBlack;
    
    [SerializeField]
    Animator mouthAnimator;

    [SerializeField]
    AudioSource narrator;
    [SerializeField]
    AudioClip mouthClip;
    [SerializeField]
    AudioClip swallow;

    float sceneStartTime;

    AsyncOperation esophagusLoading;

    void Start()
    {
        sceneStartTime = Time.time;

        StartCoroutine(explanation());
    }

    void Update()
    {
        fadeToBlack.color = new Color(0f, 0f, 0f, 1f-(Time.time-sceneStartTime));
    }

    IEnumerator explanation()
    {
        yield return new WaitForSeconds(5);
        narrator.PlayOneShot(mouthClip);
        yield return new WaitForSeconds(3);
        mouthAnimator.SetBool("chewing", true);
        yield return new WaitForSeconds(3);
        mouthAnimator.SetBool("chewing", false);
        PreloadEsophagus();
        yield return new WaitForSeconds(2.8f);
        mouthAnimator.SetTrigger("swallow");
        yield return new WaitForSeconds(0.2f);
        GetComponent<Movement>().nextTarget();
        yield return new WaitForSeconds(1);
        narrator.PlayOneShot(swallow);
    }

    public void PreloadEsophagus()
    {
        esophagusLoading = SceneManager.LoadSceneAsync("Esophagus", LoadSceneMode.Additive);
    }

    public void ContinueOnEsophagusLoaded()
    {
        StartCoroutine(continueOnEsophagusLoaded());
    }

    IEnumerator continueOnEsophagusLoaded()
    {
        while(!esophagusLoading.isDone)
        {
            yield return null;
        }
        GetComponent<Movement>().nextTarget();
    }

    public void TransferToEsophagus()
    {
        Scene esophagus = SceneManager.GetSceneByName("Esophagus");
        SceneManager.MoveGameObjectToScene(GetComponent<Movement>().player.gameObject, esophagus);
        SceneManager.MoveGameObjectToScene(gameObject, esophagus);
        GameObject.Find("EsophagusStoryline").GetComponent<Movement>().nextTarget();
        Destroy(gameObject);
    }
}
