using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class ProteinsynthesisStoryline : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Movement>().enabled = true;
        GetComponent<Movement>().nextTarget();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    public void NextScene() {
        SceneManager.LoadScene("EndScene");
    }
}
