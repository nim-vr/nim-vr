using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class FaderProteinsynthesis : MonoBehaviour
{
    [SerializeField]
    Image fadeToBlack;
    [SerializeField]
    Transform waypoint;
    [SerializeField]
    Transform player;

    float startTime;

    void Start()
    {
        startTime = Time.time;
    }

    void Update()
    {
        fadeToBlack.color = new Color(0f, 0f, 0f, Math.Max((1f-(Vector3.Distance(waypoint.position, player.position)-50)/50), (1 - Time.time + startTime)));
    }
}
