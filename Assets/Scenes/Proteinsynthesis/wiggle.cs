using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
 public class wiggle : MonoBehaviour
 {
     private float movementDuration = 1.5f;
     private float waitBeforeMoving = 1f;
     private float timeOfAnimation = 10f;
     private bool hasArrived = false;
     private bool finished = false;

    void Start()
    {
        waitBeforeMoving = Random.Range(0f, 0.2f);
        movementDuration = Random.Range(0.5f, 1.5f);
        StartCoroutine(Countdown());

    }
 
     private void Update()
     {
         if(!hasArrived & !finished)
         {
             hasArrived = true;
             float randX = Random.Range(-5f, 5f);
             float randZ = Random.Range(-5f, 5f);
             float randY = Random.Range(-5f, 5f);
             
             StartCoroutine(MoveToPoint(new Vector3(randX, randY, randZ)));
         }

     }
 
     private IEnumerator MoveToPoint(Vector3 targetPos)
     {
         float timer = 0.0f;
         Vector3 startPos = transform.position;
 
         while (timer < movementDuration)
         {
             timer += Time.deltaTime;
             float t = timer / movementDuration;
             t = t * t * t * (t * (6f * t - 15f) + 10f);
             transform.position = Vector3.Lerp(startPos, targetPos, t);
 
             yield return null;
         }
         waitBeforeMoving = Random.Range(0f, 1f);
         yield return new WaitForSeconds(waitBeforeMoving);
         hasArrived = false;
     }

     private IEnumerator Countdown() {
        
        while(true) {
            int index = int.Parse((gameObject.name).Substring((gameObject.name).Length - 2));

            yield return new WaitForSeconds(timeOfAnimation);
            hasArrived = true;
            finished = true;
            Vector3 cell_position = GameObject.Find("Cell").transform.position;
            movementDuration = 4.0f;
            StartCoroutine(MoveToPoint(cell_position + new Vector3(index-5,Random.Range(-1f, 1f),0)));

            yield return new WaitForSeconds(4);

            movementDuration = 0.1f;
            StartCoroutine(MoveToPoint(cell_position + new Vector3(index-5,0,0)));

            yield return new WaitForSeconds(1);
            
            movementDuration = 4.0f;
            yield return new WaitForSeconds((float)(index*0.1));
            StartCoroutine(MoveToPoint(new Vector3((float)(index*0.7-7),0,(float)(index*0.7))));
            
            hasArrived = true;
            finished = true;
            
            break;
            //yield return new WaitForSeconds();
        }
    }

 }