using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class VRButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{


    [SerializeField]
    UnityEvent action;

    [SerializeField]
    Image background, fill;
    [SerializeField]
    Text text;

    bool clickable = true;

    float progress;
    bool mouseOver;

    float targetAlpha = 1f;

    void Update()
    {

        const float hoverTime = 2;
        if(mouseOver && clickable)
            progress += Time.deltaTime;
        else if(clickable)
            progress -= 2*Time.deltaTime;

        if(progress < 0) progress = 0;
        else if(progress > hoverTime)
        {
            progress = hoverTime;
            clickable = false;
            targetAlpha = 0;

            action.Invoke();
        }

        GetComponent<Slider>().value = (progress / hoverTime) * (progress / hoverTime);


        const float fadeMultiplier = 2f;
        float newAlpha = background.color.a;
        if(background.color.a < targetAlpha)
            newAlpha += Time.deltaTime * fadeMultiplier;
        else if(background.color.a > targetAlpha)
            newAlpha -= Time.deltaTime * fadeMultiplier;

        if(newAlpha > 1) newAlpha = 1;
        else if(newAlpha < 0)
        {
            newAlpha = 0;
            foreach(Transform child in transform)
            {
                child.gameObject.SetActive(false);
            }
        }

        background.color = new Color(background.color.r, background.color.g, background.color.b, newAlpha);
        fill.color = new Color(fill.color.r, fill.color.g, fill.color.b, newAlpha);
        text.color = new Color(text.color.r, text.color.g, text.color.b, newAlpha);

    }

    public void reset()
    {
        progress = 0;
        clickable = true;
        targetAlpha = 100;
        foreach(Transform child in transform)
        {
            child.gameObject.SetActive(true);
        }
    }

    public void OnPointerEnter(PointerEventData data)
    {
        mouseOver = true;
    }

    public void OnPointerExit(PointerEventData data)
    {
        mouseOver = false;
    }

}
