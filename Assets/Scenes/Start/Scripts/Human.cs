using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Human : MonoBehaviour
{

    Animator animator;
    Transform tf;

    [SerializeField]
    Transform player;

    [SerializeField]
    Transform mouth;

    [SerializeField]
    Image fadeToBlack;

    void Start()
    {
        animator = GetComponent<Animator>();
        tf = GetComponent<Transform>();
    }

    void Update()
    {
        fadeToBlack.color = new Color(0f, 0f, 0f, 1f-(Vector3.Distance(mouth.position, player.position)-0.02f)*4);
        if(Vector3.Distance(mouth.position, player.position) < 4)
        {
            animator.SetBool("open", true);
        }
    }
}
