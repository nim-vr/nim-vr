using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Food : MonoBehaviour
{

    [SerializeField]
    Transform player;
    [SerializeField]
    Transform center;

    Material mat;

    void Start()
    {
        mat = GetComponent<MeshRenderer>().material;
    }

    void Update()
    {
        float dist = Vector3.Distance(center.position, player.position);
        mat.color = new Color(1f, 1f, 1f, dist*1.5f - 2f);
    }
}
