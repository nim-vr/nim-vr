using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EsophagusStoryline : MonoBehaviour
{
    [SerializeField]
    Movement mov;

    AsyncOperation stomachLoading;

    void Start()
    {
        mov.player = GameObject.Find("Player").transform;
        mov.enabled = true;
    }

    public void UnloadAndPreload()
    {
        SceneManager.UnloadSceneAsync("Mouth");
        stomachLoading = SceneManager.LoadSceneAsync("Stomach", LoadSceneMode.Additive);
    }

    public void ContinueOnStomachLoaded()
    {
        StartCoroutine(continueOnStomachLoaded());
    }

    IEnumerator continueOnStomachLoaded()
    {
        while(!stomachLoading.isDone)
        {
            yield return null;
        }
        mov.nextTarget();
    }

    public void TransferToStomach()
    {
        Scene stomach = SceneManager.GetSceneByName("Stomach");
        SceneManager.MoveGameObjectToScene(mov.player.gameObject, stomach);
        SceneManager.MoveGameObjectToScene(gameObject, stomach);
        GameObject.Find("StomachStoryline").GetComponent<Movement>().nextTarget();
        Destroy(gameObject);
    }
}
