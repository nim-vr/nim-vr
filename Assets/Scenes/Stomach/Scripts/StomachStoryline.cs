using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StomachStoryline : MonoBehaviour
{
    [SerializeField]
    Animator stomachAnimator;
    [SerializeField]
    GameObject explanationSet;

    [SerializeField]
    AudioSource narrator;

    Movement mov;
    
    AsyncOperation gutLoading;
    
    // Start is called before the first frame update
    void Start()
    {
        mov = GetComponent<Movement>();
        mov.player = GameObject.Find("Player").transform;
        mov.enabled = true;
        narrator = GameObject.Find("Narrator").GetComponent<AudioSource>();
        SetUpperMuscleOpen(true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void SetUpperMuscleOpen(bool state) {
        stomachAnimator.SetBool("OpenUpper", state);
    }
    public void SetWorking(bool state) {
        stomachAnimator.SetBool("StartWorking", state);
    }
    public void SetLowerMuscleOpen(bool state) {
        stomachAnimator.SetBool("OpenLower", state);
    }


    public void StartExplanation()
    {
        StartCoroutine(explanation());
    }

    IEnumerator explanation()
    {
        yield return new WaitForSeconds(6);
        explanationSet.SetActive(true);
        yield return new WaitForSeconds(18);
        GetComponent<Movement>().nextTarget();
    }

    public void NarratorPlay(AudioClip clipToPlay)
    {
        narrator.PlayOneShot(clipToPlay);
    }

    public void UnloadEsophagus()
    {
        SceneManager.UnloadSceneAsync("Esophagus");
    }

    public void PreloadGut()
    {
        gutLoading = SceneManager.LoadSceneAsync("Gut", LoadSceneMode.Additive);
    }

    IEnumerator continueOnGutLoaded()
    {
        while(!gutLoading.isDone)
        {
            yield return null;
        }
        mov.nextTarget();
    }

    public void TransferToGut()
    {
        Scene gut = SceneManager.GetSceneByName("Gut");
        SceneManager.MoveGameObjectToScene(mov.player.gameObject, gut);
        SceneManager.MoveGameObjectToScene(gameObject, gut);
        GameObject.Find("GutStoryline").GetComponent<Movement>().nextTarget();
        Destroy(gameObject);
    }

}
