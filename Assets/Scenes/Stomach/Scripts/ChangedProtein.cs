using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangedProtein : MonoBehaviour
{
    private float movementDuration = 2f;
    private bool hasArrived = false;
    Renderer test;

    void Start()
    {
        StartCoroutine(Countdown());
    }
 
     private void Update()
     {
         if(!hasArrived)
         {
            hasArrived = true;
            StartCoroutine(MoveToPoint(transform.position + new Vector3(0, (float)(-1), 0)));
            StartCoroutine(MoveToPointInCube(new Vector3(0, (float)(-1.5), 0)));
         }

         
     }
 
     private IEnumerator MoveToPoint(Vector3 targetPos)
     {
         float timer = 0.0f;
         Vector3 startPos = transform.position;
 
         while (timer < movementDuration)
         {
             timer += Time.deltaTime;
             float t = timer / movementDuration;
             t = t * t * t * (t * (6f * t - 15f) + 10f);
             transform.position = Vector3.Lerp(startPos, targetPos, t);
 
             yield return null;
         }
         // waitBeforeMoving = Random.Range(0f, 1f);
         yield return new WaitForSeconds(1);

     }

     private IEnumerator MoveToPointInCube(Vector3 targetPos)
     {

        yield return new WaitForSeconds(5);
        movementDuration = 3f;

         float timer = 0.0f;
         Vector3 startPos = transform.position;
         Vector3 target = targetPos + transform.position;
    
         while (timer < movementDuration)
         {
             timer += Time.deltaTime;
             float t = timer / movementDuration;
             t = t * t * t * (t * (6f * t - 15f) + 10f);
             transform.position = Vector3.Lerp(startPos, target, t);
 
             yield return null;
         }
         // waitBeforeMoving = Random.Range(0f, 1f);
         

     }

     private IEnumerator Countdown() {
        
        while(true) {
            
            yield return new WaitForSeconds(10);
            test= GetComponent<MeshRenderer>();
            test.enabled= false;
            
            break;
        }
    }
}


