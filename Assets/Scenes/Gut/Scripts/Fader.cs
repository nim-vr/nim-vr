using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Fader : MonoBehaviour
{
    [SerializeField]
    Image fadeToBlack;
    [SerializeField]
    Transform waypoint;
    Transform player;

    void Start()
    {
        player = GameObject.Find("Player").transform;
        GetComponent<Canvas>().worldCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
    }

    void Update()
    {
        fadeToBlack.color = new Color(0f, 0f, 0f, 1f-(Vector3.Distance(waypoint.position, player.position)-0.75f)*4);
    }
}
