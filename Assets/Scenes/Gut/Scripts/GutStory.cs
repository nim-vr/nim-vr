using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GutStory : MonoBehaviour
{

    [SerializeField]
    GameObject explanationSet;
    
    [SerializeField]
    GameObject explanationSet2;
    
    AudioSource narrator;

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Movement>().player = GameObject.Find("Player").transform;
        GetComponent<Movement>().enabled = true;
        narrator = GameObject.Find("Narrator").GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UnloadStomach()
    {
        SceneManager.UnloadSceneAsync("Stomach");
    }

    public void NextScene()
    {
        SceneManager.LoadScene("BloodVessel");
    }
    
    public void NarratorRead(AudioClip ac) {
        narrator.PlayOneShot(ac);
    }

    public void StartExplanation()
    {
        StartCoroutine(explanation());
    }

    IEnumerator explanation()
    {
        explanationSet.SetActive(true);
        yield return new WaitForSeconds(15);
        GetComponent<Movement>().nextTarget();
    }
    
    public void StartExplanation2()
    {
        StartCoroutine(explanation2());
    }

    IEnumerator explanation2()
    {
        explanationSet2.SetActive(true);
        yield return new WaitForSeconds(15);
        GetComponent<Movement>().nextTarget();
    }
}
