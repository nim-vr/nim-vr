using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class animation_trypsin : MonoBehaviour
{
    Renderer test;
    private float movementDuration = 4f;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(WaitAndUpdate());
        StartCoroutine(MoveToPoint(transform.position + new Vector3(2f, -2f, 0f)));
    }


    // Update is called once per frame
    void Update()
    {

    }

    private IEnumerator WaitAndUpdate()
     {
         float timer = 0.0f;
         
         yield return new WaitForSeconds(10);

        //test.enabled = true;

        Vector3 startPos = transform.position;
        Vector3 targetPos = startPos + new Vector3(1f, 3.0f, 1f);
 
        while (timer < movementDuration)
        {
             timer += Time.deltaTime;
             float t = timer / movementDuration;
             t = t * t * t * (t * (6f * t - 15f) + 10f);
             transform.position = Vector3.Lerp(startPos, targetPos, t);
 
             yield return null;
        }
     }

     private IEnumerator MoveToPoint(Vector3 targetPos)
     {
         float timer = 0.0f;
         Vector3 startPos = transform.position;
 
         while (timer < movementDuration)
         {
             timer += Time.deltaTime;
             float t = timer / movementDuration;
             t = t * t * t * (t * (6f * t - 15f) + 10f);
             transform.position = Vector3.Lerp(startPos, targetPos, t);
 
             yield return null;
         }
     }
}
