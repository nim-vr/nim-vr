using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class FaderBloodVessel : MonoBehaviour
{
    [SerializeField]
    Image fadeToBlack;
    [SerializeField]
    Transform fadingReference;
    [SerializeField]
    Transform fadingOutReference;
    [SerializeField]
    Transform player;

    void Start()
    {
        player = GameObject.Find("Player").transform;
        GetComponent<Canvas>().worldCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
    }

    void Update()
    {
        fadeToBlack.color = new Color(0f, 0f, 0f, 1f-(Math.Min(Vector3.Distance(fadingReference.position, player.position), Vector3.Distance(fadingOutReference.position, player.position))-0.05f));
    }
    
    public void NextScene() {
        SceneManager.LoadScene("Protein_Synthese");
    }
}
