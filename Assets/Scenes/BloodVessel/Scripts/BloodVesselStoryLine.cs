using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BloodVesselStoryLine : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Movement>().player = GameObject.Find("Player").transform;
        GetComponent<Movement>().enabled = true;
        GetComponent<Movement>().nextTarget();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
