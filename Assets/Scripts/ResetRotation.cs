using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetRotation : MonoBehaviour
{
    [SerializeField]
    Transform offset;
    [SerializeField]
    Transform cameraTrf;

    public void Start()
    {
        StartCoroutine(centerCamera());
    }

    public IEnumerator centerCamera()
    {
        if(cameraTrf.eulerAngles == Vector3.zero)
            yield return null;

        offset.eulerAngles = new Vector3(
            0,
            - cameraTrf.eulerAngles.y,
            0
        );
    }
}
