using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Movement : MonoBehaviour
{
    
    public Transform player;

    enum MovementType
    {
        Linear,
        Smooth,
        SmoothIn,
        SmoothOut
    }

    [System.Serializable]
    struct target
    {
        public float movingTime;
        public Transform endPosition;
        public float targetDistance;
        public MovementType movementType;
        public UnityEvent postAction;
    }

    [SerializeField]
    target[] targets;
    int currentTarget = 0;

    Vector3 currentMovingSpeed = Vector3.zero;

    Vector3 startPosition = Vector3.zero;
    float startTime = 0f;

    int done = -1;


    public void nextTarget()
    {
        if(targets.Length > currentTarget + 1)
            ++currentTarget;
        startPosition = player.position;
        startTime = Time.time;
        done = -1;
    }

    public void Update()
    {

        target curr =  targets[currentTarget];

        if(curr.movingTime != 0f)
        {
            if(Vector3.Distance(curr.endPosition.position, player.position) > curr.targetDistance)
            {
                switch (curr.movementType)
                {
                    case MovementType.Linear:
                        player.position = Vector3.Lerp(startPosition, curr.endPosition.position, (Time.time - startTime) / curr.movingTime);
                        break;
                    case MovementType.Smooth:
                        player.position = Vector3.SmoothDamp(player.position, curr.endPosition.position, ref currentMovingSpeed, curr.movingTime);
                        break;
                    case MovementType.SmoothIn:
                        player.position = Vector3.SmoothDamp(player.position, curr.endPosition.position + (curr.endPosition.position - startPosition), ref currentMovingSpeed, curr.movingTime);
                        break;
                    case MovementType.SmoothOut:
                        player.position += ((curr.endPosition.position - player.position) * Time.fixedDeltaTime / curr.movingTime);
                        break;
                }
            }
            else
            {
                if(done != currentTarget)
                {
                    done = currentTarget;
                    curr.postAction.Invoke();
                }
            }
        }

    }
}
